# Distributed CW2

## Name
Album Colour Picker and LED Colour setter
Jamal Ahmed and Mitali Sen

## Description
service1 - API service that takes a song name and relevant artist name and returns a URL safe base64 encoded string representing the album image in jpg format
service2 - API service that takes a URL safe base64 encoded string of a jpeg and decodes it. Then chooses the discrete colours from the image and returns them in JSON format. Colours are in HSV format
service 3 - Blynk API to set the LEDS in my living room to the relevant colours

## Installation
python 3.8
pip3 install colorsys
pip3 install requests
pip3 install flask_restful
pip3 insall Flask
pip3 install extcolors uses PIL

## Usage
python3 service1.py in one terminal/computer
python3 Service2.py in another terminal/computer
python3 webapp.py in another terminal/computer

service 1 - localhost:5000
service 2 - localhost:6000
web app - localhost:7000

on localhost:7000 input song and artist name and click submit
