from requests import get, put,post ,delete
import time
import statistics


def sendBlynkRequest(pin,values):
	URL = "https://blynk.cloud/external/api/update?token=vgiM6bK4hb75CkO7G_m-yT9E5N2lsK0W&"+pin+"="+str(float(values[0])*255)
	return get(URL)

TimeFile = open("TimeToRun.txt","a")
a = [[],[],[],[],[],[]]

for i in range(0,10):
    URL = "http://127.0.0.1"
    start = time.time()
    AllSongs = get(URL+":5000")
    TimeFile.write("Time to get All available songs - "+str(time.time()-start))
    TimeFile.write("\n")
    a[0].append(float(time.time()-start))

    start = time.time()
    GetSong = get(URL+":5000/batman/jaden")
    TimeFile.write("Time to get album image- "+str(time.time()-start))
    TimeFile.write("\n")
    a[1].append(float(time.time()-start))

    start = time.time()
    PostImage = post(URL+":6000/"+GetSong.text)
    PhotoID = PostImage.text.replace("\n","")
    PhotoID = PhotoID.replace('"',"")
    TimeFile.write("Time to post image to service2 - "+str(time.time()-start))
    TimeFile.write("\n")
    a[2].append(float(time.time()-start))

    start = time.time()
    GetColours = get(URL+":6000/"+ PhotoID +"/7").json()
    TimeFile.write("Time to get colours from service2 - "+str(time.time()-start))
    TimeFile.write("\n")
    a[3].append(float(time.time()-start))

    start = time.time()
    DelImage = delete(URL+":6000/delete/"+PhotoID)
    TimeFile.write("Time to delete image from service2 - "+str(time.time()-start))
    TimeFile.write("\n")
    a[4].append(float(time.time()-start))

    start = time.time()
    for i in range(0,7):
        Col = GetColours[i][str(i)]["Colour"]
        setHue = sendBlynkRequest("V4"+str(i),[str(Col['H']),str(Col["S"]),str(Col['V'])])
        setSat = sendBlynkRequest("V5"+str(i),[str(Col["S"]),str(Col['V'])])
    TimeFile.write("Time to set LED Colours - "+str(time.time()-start))
    TimeFile.write("\n")
    TimeFile.write("\n")
    a[5].append(float(time.time()-start))

reqnum = 1
for service in a:
    average = 0
    for t in service:
        average+=t
    TimeFile.write("Request "+str(reqnum)+" statistics\n")
    TimeFile.write("Average -  "+str(average/len(service)))
    TimeFile.write("\n")
    TimeFile.write("Std dev - " +str(statistics.stdev(service)))
    TimeFile.write("\n")
    TimeFile.write("\n")
    reqnum+=1

TimeFile.close()
