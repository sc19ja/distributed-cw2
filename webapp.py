from flask import Flask, render_template, request
from requests import get, put,post ,delete
import base64
import os
from datetime import datetime
import colorsys
import ast
app = Flask(__name__)

def sendBlynkRequest(pin,values):
	URL = "https://blynk.cloud/external/api/update?token=vgiM6bK4hb75CkO7G_m-yT9E5N2lsK0W&"+pin+"="+str(float(values[0])*255)
	return get(URL)

def Base64URLDecoder(encoded):
	decoded = encoded.replace("-","+")
	decoded = decoded.replace("|","/")
	decoded = decoded.replace("_","=")
	decoded = decoded.replace('"',"")
	return bytes(decoded,'utf-8')

@app.route("/", methods=["POST", "GET"])
def home():
	songs = []
	artists = []
	try:
		response = get("http://localhost:5000/")
		serverData = response.json()
		for item in serverData:
			songs.append(item['song'])
			artists.append(item['artist'])
	except:
		return render_template("home.html",rgb=[],SongData = songs,ArtistData = artists,error="Service 1(Album art finder) didn't respond with available songs")

	if request.method == "POST":
		artist = request.form['artist']
		song = request.form['song']

		imagedata = ""
		# try:
		response = get("http://127.0.0.1:5000/" + song + "/" + artist)
		if response.status_code == 200:
			imagedata = response.text
		else:
			return render_template("home.html",error="Service 1(Album art finder) returned an error: "+ast.literal_eval(response.text)["description"],SongData = songs,ArtistData = artists)
		# except:
		# 		return render_template("home.html",rgb=[],error="Service 1(Album art finder) didn't respond",SongData = songs,ArtistData = artists)
		try:
			response2 = post("http://127.0.0.1:6000/"+imagedata)
			if response2.status_code == 200:
				PhotoID = response2.text.replace("\n","")
				PhotoID = PhotoID.replace('"',"")
				response2ii = get("http://127.0.0.1:6000/"+str(PhotoID)+"/7")
				if response2ii.status_code == 200:
					delete("http://127.0.0.1:6000/delete/"+str(PhotoID))
					Colours = response2ii.json()
					RGB = []
					for i in range(0,7):
						Col = Colours[i][str(i)]["Colour"]
						try:
							RGB.append(colorsys.hsv_to_rgb(Col['H'],Col["S"],Col['V']))
							setHue = sendBlynkRequest("V4"+str(i),[str(Col['H']),str(Col["S"]),str(Col['V'])])
							setSat = sendBlynkRequest("V5"+str(i),[str(Col["S"]),str(Col['V'])])
						except:
							return render_template("home.html",error="Blynk Server didn't respond",SongData = songs,ArtistData = artists)
					offset = datetime.now().time().strftime("%d%m%Y%H%M%S%f")
					#remove previous images
					for file in os.listdir("./static/"):
						os.remove("./static/"+file)
					f = open("./static/current"+ str(offset) +".jpg","wb")
					f.write(base64.decodebytes(Base64URLDecoder(imagedata)))
					f.close()
					return render_template("home.html",image = "./static/current"+ str(offset) +".jpg",rgb = RGB,SongData = songs,ArtistData = artists)
				else:
					return render_template("home.html",error="Service 2(Colour picker) didn't return colours",SongData = songs,ArtistData = artists)
			else:
				return render_template("home.html",error="Service 2(Colour picker) returned an error",SongData = songs,ArtistData = artists)
		except:
			return render_template("home.html",error="Service 2(Colour picker) didn't respond",SongData = songs,ArtistData = artists)


	return render_template("home.html",rgb=[],SongData = songs,ArtistData = artists,error="")


if __name__ == "__main__":
	# app.run("192.168.0.16",port=7000)
	app.run(debug=True,port=7000)
