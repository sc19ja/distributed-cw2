from flask import Flask, request
from flask_restful import Resource, Api, abort
import base64
import extcolors
import colorsys
from math import sqrt
import os
import ast

app = Flask(__name__)
api = Api(app)
Resources = {1:["c1","c2"]}

def Base64URLDecoder(encoded):
	decoded = encoded.replace("-","+")
	decoded = decoded.replace("|","/")
	decoded = decoded.replace("_","=")
	return bytes(decoded,'utf-8')

class ColourPicker(Resource):
	#get colours from photoID
	def get(self,id,numColours):
		if id in Resources.keys():
			#make it so there are numColours
			Formatted = Resources[id]
			appender = 0
			if len(Formatted) ==0:
				Formatted = [(255,0,0),(0,255,0),(0,0,255),(255,0,255),(255,255,0),(0,255,255)]

			while len(Formatted)<numColours:
				Formatted.append(Formatted[appender])
				appender+= 1

			Data = []
			for i in range(0,len(Formatted)):
				Data.append({i:{"Colour":{"H":Formatted[i][0],"S":Formatted[i][1],"V":Formatted[i][2]}}})
			return Data
		else:
			return []

	def post(self,ImageData):
		id = int(list(Resources.keys())[-1])+1
		id = str(id)
		try:
			f = open("./Service2Images/"+id+".jpg","wb")
			f.write(base64.decodebytes(Base64URLDecoder(ImageData)))
			f.close()
			Colours = []
			#opens image with colours and count
			colours, pixel_count = extcolors.extract_from_path("./Service2Images/"+id+".jpg")
			#removes the count
			for colour in colours:
				Colours.append(colour[0])
			#convert colours to hsv for colour maths
			HSVColours = []
			for c in Colours:
				HSVColours.append(colorsys.rgb_to_hsv(c[0], c[1], c[2]))
			Resources[id] = HSVColours
			return id
		except:
			abort(410,description="Error decoding image")

	def delete(self,id):
		if os.path.exists("./Service2Images/"+id+".jpg"):
			os.remove("./Service2Images/"+id+".jpg")
			return "Image Deleted"
		else:
			abort(410, description= "Image with ID - "+ id +" does not exist on the server")

api.add_resource(ColourPicker, '/<string:ImageData>','/<string:id>/<int:numColours>','/delete/<string:id>')

if __name__ == '__main__':
	app.run("127.0.0.1",port=6000)
