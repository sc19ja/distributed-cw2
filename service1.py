import base64
from flask import Flask
from flask_restful import Resource, Api, abort
import os

app = Flask(__name__)
api = Api(app)

# encoding to make the string url safe
def Base64URLEncoder(raw):
    encoded = str(raw)[2:-1]
    encoded = encoded.replace("+", "-")
    encoded = encoded.replace("/", "|")
    encoded = encoded.replace("=", "_")
    return encoded

# converting the album image to base64 to send to the second service
class Artwork(Resource):
    def get(self, song, artist):
        path = "./images/" + song.lower() + "_" + artist.lower() + ".jpg"
        if not os.path.exists(path):
            abort(410, description=song+ " by " + artist +" does not exist on the server. Please use options from drop down menu")
        with open(path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())
        return Base64URLEncoder(encoded_string)

# listing the available songs with the artist name for the user
class Albums(Resource):
    def get(self):
        data = []
        for filename in os.listdir("./images/"):
            f = filename.replace(".jpg", "")
            f = f.split("_")
            data.append({"song": f[0], "artist": f[1]})
        return data


api.add_resource(Artwork, '/<string:song>/<string:artist>')
api.add_resource(Albums, '/')

if __name__ == '__main__':
    app.run("127.0.0.1", port=5000)
